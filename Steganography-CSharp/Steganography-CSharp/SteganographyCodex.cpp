/**************************************************
 * Dr. Boatright's .cpp and class declaration for Steganography Codex
 * getWidth, getHeight, and BMP_Handler DLL written by Samuel Kenney
****************************************************/
#include "SteganographyCodex.h"
#include <sstream>

using namespace std;

SteganographyCodex::SteganographyCodex(const string& filename) {
	rawBytes = nullptr;
	rawBytes = BMP_Handler::loadBMP(filename.c_str(), width, height);
}


SteganographyCodex::~SteganographyCodex() {
	if(rawBytes) {
		delete [] rawBytes;
	}
}

string SteganographyCodex::getMessage() {
	const unsigned char magicNumber[4] = "316";
	const unsigned char mask = 0x1;
	unsigned int currCharIndex = 0;	//"iterator"
	unsigned char constructChar;
	
	//read enough to check for magic number
	unsigned char buffer[3];
	for(int i = 0; i < 3; i++) {
		constructChar = 0;
		for(int j = 0; j < 8; j++) {
			//to read a bit use and, to write a bit use or
			constructChar |= (rawBytes[currCharIndex++] & mask) << j;
		}
		buffer[i] = constructChar;
	}

	if(buffer[0] != magicNumber[0] || buffer[1] != magicNumber[1] || buffer[2] != magicNumber[2]) {
		return "";
	}

	ostringstream out;

	do {
		constructChar = 0;
		for(int i = 0; i < 8; i++) {
			constructChar |= (rawBytes[currCharIndex++] & mask) << i;
		}
		out << constructChar;
	} while(constructChar != '\0');

	return out.str();
}

void SteganographyCodex::setMessage(const string& message) {
	const unsigned char magicNumber[4] = "316";
	const unsigned char mask = 0x1;
	const unsigned char stripMask = 0xfe;	//only the last bit is a 0
	unsigned int currCharIndex = 0;	//"iterator"

	//write the magic number
	for(int i = 0; i < 3; i++) {
		for(int j = 0; j < 8; j++) {
			//strip the last bit of the current byte
			rawBytes[currCharIndex] &= stripMask;

			//to read a bit use and, to write a bit use or
			rawBytes[currCharIndex] |= (magicNumber[i] & (mask << j)) ? 1 : 0;

			currCharIndex++;
		}
	}

	//write the rest of it, make sure to null-terminate
	for(unsigned int i = 0; i < message.length(); i++) {
		for(int j = 0; j < 8; j++) {
			//strip the last bit of the current byte
			rawBytes[currCharIndex] &= stripMask;

			//to read a bit use and, to write a bit use or
			rawBytes[currCharIndex] |= (message[i] & (mask << j)) ? 1 : 0;

			currCharIndex++;
		}
	}

	for(int j = 0; j < 8; j++) {
		//strip the last bit of the current byte 8 times to create the terminal null
		rawBytes[currCharIndex] &= stripMask;
	
		currCharIndex++;
	}
}

void SteganographyCodex::writeImage(const string& filename) {
	BMP_Handler::saveBMP(filename.c_str(), rawBytes, width, height);
}