﻿namespace Steganography_CSharp
{
    partial class Stegonagraphy
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Stegonagraphy));
            this.OpenBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.CloseBtn = new System.Windows.Forms.Button();
            this.ExitBtn = new System.Windows.Forms.Button();
            this.BitmapPictureBox = new System.Windows.Forms.PictureBox();
            this.MessageTextBox = new System.Windows.Forms.RichTextBox();
            this.ReadBtn = new System.Windows.Forms.Button();
            this.WriteBtn = new System.Windows.Forms.Button();
            this.AmountLeftLbl = new System.Windows.Forms.Label();
            this.TwoBtn = new System.Windows.Forms.Button();
            this.ThreeBtn = new System.Windows.Forms.Button();
            this.FourBtn = new System.Windows.Forms.Button();
            this.FiveBtn = new System.Windows.Forms.Button();
            this.SixBtn = new System.Windows.Forms.Button();
            this.SevenBtn = new System.Windows.Forms.Button();
            this.EightButn = new System.Windows.Forms.Button();
            this.OneBtn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.BitmapPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // OpenBtn
            // 
            this.OpenBtn.Image = ((System.Drawing.Image)(resources.GetObject("OpenBtn.Image")));
            this.OpenBtn.Location = new System.Drawing.Point(12, 13);
            this.OpenBtn.Name = "OpenBtn";
            this.OpenBtn.Size = new System.Drawing.Size(137, 46);
            this.OpenBtn.TabIndex = 0;
            this.OpenBtn.UseVisualStyleBackColor = true;
            this.OpenBtn.Click += new System.EventHandler(this.OpenBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Enabled = false;
            this.SaveBtn.Image = ((System.Drawing.Image)(resources.GetObject("SaveBtn.Image")));
            this.SaveBtn.Location = new System.Drawing.Point(155, 13);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(138, 46);
            this.SaveBtn.TabIndex = 1;
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // CloseBtn
            // 
            this.CloseBtn.Enabled = false;
            this.CloseBtn.Image = ((System.Drawing.Image)(resources.GetObject("CloseBtn.Image")));
            this.CloseBtn.Location = new System.Drawing.Point(299, 13);
            this.CloseBtn.Name = "CloseBtn";
            this.CloseBtn.Size = new System.Drawing.Size(157, 46);
            this.CloseBtn.TabIndex = 2;
            this.CloseBtn.UseVisualStyleBackColor = true;
            this.CloseBtn.Click += new System.EventHandler(this.CloseBtn_Click);
            // 
            // ExitBtn
            // 
            this.ExitBtn.Image = ((System.Drawing.Image)(resources.GetObject("ExitBtn.Image")));
            this.ExitBtn.Location = new System.Drawing.Point(462, 13);
            this.ExitBtn.Name = "ExitBtn";
            this.ExitBtn.Size = new System.Drawing.Size(155, 46);
            this.ExitBtn.TabIndex = 3;
            this.ExitBtn.UseVisualStyleBackColor = true;
            this.ExitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // BitmapPictureBox
            // 
            this.BitmapPictureBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.BitmapPictureBox.Image = ((System.Drawing.Image)(resources.GetObject("BitmapPictureBox.Image")));
            this.BitmapPictureBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("BitmapPictureBox.InitialImage")));
            this.BitmapPictureBox.Location = new System.Drawing.Point(13, 66);
            this.BitmapPictureBox.Name = "BitmapPictureBox";
            this.BitmapPictureBox.Size = new System.Drawing.Size(603, 422);
            this.BitmapPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.BitmapPictureBox.TabIndex = 4;
            this.BitmapPictureBox.TabStop = false;
            this.BitmapPictureBox.Click += new System.EventHandler(this.BitmapPictureBox_Click);
            // 
            // MessageTextBox
            // 
            this.MessageTextBox.Enabled = false;
            this.MessageTextBox.Location = new System.Drawing.Point(13, 507);
            this.MessageTextBox.Name = "MessageTextBox";
            this.MessageTextBox.Size = new System.Drawing.Size(457, 112);
            this.MessageTextBox.TabIndex = 5;
            this.MessageTextBox.Text = "";
            this.MessageTextBox.TextChanged += new System.EventHandler(this.MessageTextBox_TextChanged);
            // 
            // ReadBtn
            // 
            this.ReadBtn.Enabled = false;
            this.ReadBtn.Image = ((System.Drawing.Image)(resources.GetObject("ReadBtn.Image")));
            this.ReadBtn.Location = new System.Drawing.Point(476, 507);
            this.ReadBtn.Name = "ReadBtn";
            this.ReadBtn.Size = new System.Drawing.Size(140, 54);
            this.ReadBtn.TabIndex = 6;
            this.ReadBtn.UseVisualStyleBackColor = true;
            this.ReadBtn.Click += new System.EventHandler(this.ReadBtn_Click);
            // 
            // WriteBtn
            // 
            this.WriteBtn.Enabled = false;
            this.WriteBtn.Image = ((System.Drawing.Image)(resources.GetObject("WriteBtn.Image")));
            this.WriteBtn.Location = new System.Drawing.Point(476, 567);
            this.WriteBtn.Name = "WriteBtn";
            this.WriteBtn.Size = new System.Drawing.Size(140, 52);
            this.WriteBtn.TabIndex = 7;
            this.WriteBtn.UseVisualStyleBackColor = true;
            this.WriteBtn.Click += new System.EventHandler(this.WriteBtn_Click);
            // 
            // AmountLeftLbl
            // 
            this.AmountLeftLbl.AutoSize = true;
            this.AmountLeftLbl.Location = new System.Drawing.Point(13, 491);
            this.AmountLeftLbl.Name = "AmountLeftLbl";
            this.AmountLeftLbl.Size = new System.Drawing.Size(24, 13);
            this.AmountLeftLbl.TabIndex = 8;
            this.AmountLeftLbl.Text = "0/0";
            this.AmountLeftLbl.Click += new System.EventHandler(this.AmountLeftLbl_Click);
            // 
            // TwoBtn
            // 
            this.TwoBtn.Enabled = false;
            this.TwoBtn.Image = ((System.Drawing.Image)(resources.GetObject("TwoBtn.Image")));
            this.TwoBtn.Location = new System.Drawing.Point(89, 626);
            this.TwoBtn.Name = "TwoBtn";
            this.TwoBtn.Size = new System.Drawing.Size(71, 54);
            this.TwoBtn.TabIndex = 10;
            this.TwoBtn.UseVisualStyleBackColor = true;
            this.TwoBtn.Click += new System.EventHandler(this.TwoBtn_Click);
            // 
            // ThreeBtn
            // 
            this.ThreeBtn.Enabled = false;
            this.ThreeBtn.Image = ((System.Drawing.Image)(resources.GetObject("ThreeBtn.Image")));
            this.ThreeBtn.Location = new System.Drawing.Point(166, 625);
            this.ThreeBtn.Name = "ThreeBtn";
            this.ThreeBtn.Size = new System.Drawing.Size(68, 54);
            this.ThreeBtn.TabIndex = 11;
            this.ThreeBtn.UseVisualStyleBackColor = true;
            this.ThreeBtn.Click += new System.EventHandler(this.ThreeBtn_Click);
            // 
            // FourBtn
            // 
            this.FourBtn.Enabled = false;
            this.FourBtn.Image = ((System.Drawing.Image)(resources.GetObject("FourBtn.Image")));
            this.FourBtn.Location = new System.Drawing.Point(240, 626);
            this.FourBtn.Name = "FourBtn";
            this.FourBtn.Size = new System.Drawing.Size(69, 54);
            this.FourBtn.TabIndex = 12;
            this.FourBtn.UseVisualStyleBackColor = true;
            this.FourBtn.Click += new System.EventHandler(this.FourBtn_Click);
            // 
            // FiveBtn
            // 
            this.FiveBtn.Enabled = false;
            this.FiveBtn.Image = ((System.Drawing.Image)(resources.GetObject("FiveBtn.Image")));
            this.FiveBtn.Location = new System.Drawing.Point(315, 626);
            this.FiveBtn.Name = "FiveBtn";
            this.FiveBtn.Size = new System.Drawing.Size(71, 54);
            this.FiveBtn.TabIndex = 13;
            this.FiveBtn.UseVisualStyleBackColor = true;
            this.FiveBtn.Click += new System.EventHandler(this.FiveBtn_Click);
            // 
            // SixBtn
            // 
            this.SixBtn.Enabled = false;
            this.SixBtn.Image = ((System.Drawing.Image)(resources.GetObject("SixBtn.Image")));
            this.SixBtn.Location = new System.Drawing.Point(392, 625);
            this.SixBtn.Name = "SixBtn";
            this.SixBtn.Size = new System.Drawing.Size(64, 54);
            this.SixBtn.TabIndex = 14;
            this.SixBtn.UseVisualStyleBackColor = true;
            this.SixBtn.Click += new System.EventHandler(this.SixBtn_Click);
            // 
            // SevenBtn
            // 
            this.SevenBtn.Enabled = false;
            this.SevenBtn.Image = ((System.Drawing.Image)(resources.GetObject("SevenBtn.Image")));
            this.SevenBtn.Location = new System.Drawing.Point(462, 626);
            this.SevenBtn.Name = "SevenBtn";
            this.SevenBtn.Size = new System.Drawing.Size(70, 54);
            this.SevenBtn.TabIndex = 15;
            this.SevenBtn.UseVisualStyleBackColor = true;
            this.SevenBtn.Click += new System.EventHandler(this.SevenBtn_Click);
            // 
            // EightButn
            // 
            this.EightButn.Enabled = false;
            this.EightButn.Image = ((System.Drawing.Image)(resources.GetObject("EightButn.Image")));
            this.EightButn.Location = new System.Drawing.Point(539, 625);
            this.EightButn.Name = "EightButn";
            this.EightButn.Size = new System.Drawing.Size(70, 55);
            this.EightButn.TabIndex = 16;
            this.EightButn.UseVisualStyleBackColor = true;
            this.EightButn.Click += new System.EventHandler(this.EightButn_Click);
            // 
            // OneBtn
            // 
            this.OneBtn.Enabled = false;
            this.OneBtn.Image = ((System.Drawing.Image)(resources.GetObject("OneBtn.Image")));
            this.OneBtn.Location = new System.Drawing.Point(13, 626);
            this.OneBtn.Name = "OneBtn";
            this.OneBtn.Size = new System.Drawing.Size(70, 54);
            this.OneBtn.TabIndex = 17;
            this.OneBtn.UseVisualStyleBackColor = true;
            this.OneBtn.Click += new System.EventHandler(this.OneBtn_Click_1);
            // 
            // Stegonagraphy
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(629, 692);
            this.Controls.Add(this.OneBtn);
            this.Controls.Add(this.EightButn);
            this.Controls.Add(this.SevenBtn);
            this.Controls.Add(this.SixBtn);
            this.Controls.Add(this.FiveBtn);
            this.Controls.Add(this.FourBtn);
            this.Controls.Add(this.ThreeBtn);
            this.Controls.Add(this.TwoBtn);
            this.Controls.Add(this.AmountLeftLbl);
            this.Controls.Add(this.WriteBtn);
            this.Controls.Add(this.ReadBtn);
            this.Controls.Add(this.MessageTextBox);
            this.Controls.Add(this.BitmapPictureBox);
            this.Controls.Add(this.ExitBtn);
            this.Controls.Add(this.CloseBtn);
            this.Controls.Add(this.SaveBtn);
            this.Controls.Add(this.OpenBtn);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimizeBox = false;
            this.Name = "Stegonagraphy";
            this.Text = "Stegonagraphy";
            ((System.ComponentModel.ISupportInitialize)(this.BitmapPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OpenBtn;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Button CloseBtn;
        private System.Windows.Forms.Button ExitBtn;
        private System.Windows.Forms.PictureBox BitmapPictureBox;
        private System.Windows.Forms.RichTextBox MessageTextBox;
        private System.Windows.Forms.Button ReadBtn;
        private System.Windows.Forms.Button WriteBtn;
        private System.Windows.Forms.Label AmountLeftLbl;
        private System.Windows.Forms.Button TwoBtn;
        private System.Windows.Forms.Button ThreeBtn;
        private System.Windows.Forms.Button FourBtn;
        private System.Windows.Forms.Button FiveBtn;
        private System.Windows.Forms.Button SixBtn;
        private System.Windows.Forms.Button SevenBtn;
        private System.Windows.Forms.Button EightButn;
        private System.Windows.Forms.Button OneBtn;
    }
}

